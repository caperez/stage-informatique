# Stage découverte informatique

Visites de data center de l’EPFL et analyse de réseau avec «wireshark».

## Datacenter

- Visite CCT

- Réflexion par groupe de 2 ou 3 pendants 15 minutes. _Que faut-il pour faire fonctionner un datacenter ?_

## Installation d'un serveur web 
Dans un terminal faire les commandes suivantes :

`sudo apt update`

`sudo apt install apache2 php8.1-fpm php8.1 libapache2-mod-php8.1 php8.1-common php8.1-mysql php8.1-xml php8.1-xmlrpc php8.1-curl php8.1-gd php8.1-imagick php8.1-cli php8.1-imap php8.1-mbstring php8.1-opcache php8.1-soap php8.1-zip php8.1-intl php8.1-bcmath unzip -y`

Vérifier que tout fonctionne :

- Ouvrir firefox
- Aller sur le site : http://localhost
- La page "**Apache2 Default Page**" s'affiche ✅

## Installation de wireshark
Dans un terminal faire les commandes suivantes :

`sudo apt install wireshark -y`

`sudo dpkg-reconfigure wireshark-common` (yes)

`sudo adduser $USER wireshark`

### Définition :  https://fr.wikipedia.org/wiki/Wireshark
Wireshark est un analyseur de paquets libre et gratuit. Il est utilisé dans le dépannage et l’analyse de réseaux informatiques, le développement de protocoles, l’éducation et la rétro-ingénierie.

Wireshark utilise la bibliothèque logicielle Qt pour l’implémentation de son interface utilisateur et pcap pour la capture des paquets ; il fonctionne sur de nombreux environnements compatibles UNIX comme GNU/Linux, FreeBSD, NetBSD, OpenBSD ou Mac OSX, mais également sur Microsoft Windows. Il existe aussi entre autres une version en ligne de commande nommé TShark. Ces programmes sont distribués gratuitement sous la licence GNU General Public License.

Wireshark reconnaît 1 515 protocoles. 

## Téléchargement de la page web de test

Dans un terminal faire les commandes suivantes :

`sudo wget -O /var/www/html/index.html https://gitlab.epfl.ch/caperez/stage-informatique/-/raw/main/web/index.html?inline=false`

Vérifier que tout fonctionne :

- Ouvrir firefox
- Aller sur le site : http://localhost
- La page s'affiche avec un gros bouton rouge "Show password"
- Démarrer wireshark 

`sudo wireshark` 

- Qu'est-ce qu'il se passe ?
- Comment faire pour que cela soit plus lisible ?
- Trouver le mot de passe - le changer et faire des tests 

## Complexité + 1

Dans un terminal faire les commandes suivantes :

`sudo wget -O /var/www/html/level1.html https://gitlab.epfl.ch/caperez/stage-informatique/-/raw/main/web/level1.html?inline=false`

- Ouvrir firefox
- Aller sur le site : http://localhost/level1.html 
- Trouver le mot de passe - le changer et faire des tests 
- Differences ?

## Complexité + 2
Par groupe de 2 ou 3, faire l'etape **"Complexité + 1"**
- Ouvrir firefox
- Aller sur le site : http://{{CHANGETHIS}}/level1.html 
    - ⚠️{{CHANGETHIS}} = L'adresse IP de mon binôme ⚠️
    - Dans un terminal faire la commande suivante pour avoir mon adresse IP :
    `hostname -I`



J'introduis des informations sur le site de mon collègue, lui il regarde le trafic réseau.

**_Est-ce que j'arrive à deviner ce qu'il a écrit ?_**

